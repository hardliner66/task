# Task
A simple command line task tracker.

## Usage
#### use "task start" when you begin a new task
```bash
task start some task
```

#### use "task start" when you switch between tasks
```bash
task start some other task
```

#### use "task stop" when you take a break or stop working
```bash
task stop
```

#### use "task continue" to restart the last task before the stop
```bash
task continue
```

#### use "task list" to show all your tasks
```bash
task list
```

## Build
1. install dmd
2. install dub

build with:
```bash
dub build --build=release
```

## Current State
* Basic functionallity (starting, stopping, show all tasks) works allready

## Planned features
* Filter for task list