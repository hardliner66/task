import std.json;
import painlessjson;
import std.stdio;
import std.file;
import std.conv;
import std.path;
import std.string;
import std.process;
import std.datetime;


version(Windows) {
    extern(Windows) int SetConsoleOutputCP(uint);
    immutable string homeEnv = "USERPROFILE";
} else {
    immutable string homeEnv = "HOME";
}

enum TaskType {Start, Stop};

struct Task {
    string StartTime;
    string Name;
    TaskType Type;
}

Task[] tasks;

string fileName;

string appName;

void printHelp(string reason) {
    writeln(reason);
    writeln("====================");
    writeln("Usage:");
    writeln(appName ~ " init");
    writeln(appName ~ " start <task>");
    writeln(appName ~ " stop");
    writeln(appName ~ " continue");
    writeln(appName ~ " list");
    writeln(appName ~ " path");
    // filter is currently not implemented
    // writeln(appName ~ " list [startDate] [endDate]");
    writeln("====================");
    writeln("====================");
}

void load() {
    if (exists(fileName)) {
        try {
            auto f = File(fileName, "r");
            tasks = fromJSON!(Task[])(parseJSON(f.rawRead(new char[cast(uint)f.size])));
            f.close();
        } catch (Exception e) {

        }
    }
}

void save() {
    auto f = File(fileName, "w");
    f.rawWrite(tasks.toJSON.toPrettyString);
    f.flush();
    f.close();
}

string popArg(ref string[] args) {
    string result = args[0];
    args = args[1..$];
    return result;
}

string durationToString(Duration d) {
    auto duration = d.split;

    string result = "";
    string prefix = "";

    string timeToString(long value, string suffix) {
        string strValue = "";
        if (value > 0) {
            strValue = prefix ~ value.to!string ~ " " ~ suffix;
            prefix = ", ";
        }
        return strValue;
    }

    result ~= timeToString(duration.days, "Days");
    result ~= timeToString(duration.hours, "Hours");
    result ~= timeToString(duration.minutes, "Minutes");
    result ~= timeToString(duration.seconds, "Seconds");

    return result;
}

void waitForInput() {
    version(Windows) {
        if (!isatty(0))
            readln();
    }
}

void main(string[] args)
{
    version(Windows) {
        if(SetConsoleOutputCP(65001) == 0) {
            writeln("Could not set code page. Some chars might be displayed improperly.");
        }
    }

    fileName = buildPath(environment.get(homeEnv), "timetable.json");
    appName = baseName(popArg(args));
    if (args.length < 1) {
        printHelp("No arguments given");
        return;
    }

    string command = popArg(args).toLower;

    if (command == "init") {
        load();
        save();
        writeln("taskfile written to: " ~ fileName);
    } else if (command == "start") {
        if (args.length < 1) {
            printHelp("No Task Name given");
            return;
        }

        load();
        string taskName = "";
        string prefix = "";
        while (args.length > 0) {
            taskName ~= prefix ~ popArg(args);
            prefix = " ";
        }

        auto t = Task();
        t.Name = taskName;
        t.Type = TaskType.Start;
        t.StartTime = Clock.currTime().toISOExtString();
        tasks ~= t;
        save();
    } else if (command == "stop") {
        load();
        if (tasks.length >= 1 && tasks[$-1].Type == TaskType.Start) {
            auto t = Task();
            t.Name = "";
            t.Type = TaskType.Stop;
            t.StartTime = Clock.currTime().toISOExtString();
            tasks ~= t;
            save();
        } else {
            printHelp("No task running");
        }
    } else if (command == "continue") {
        load();
        if (tasks.length >= 2 && tasks[$-1].Type == TaskType.Stop) {
            auto t = tasks[$-2];
            t.StartTime = Clock.currTime().toISOExtString();
            tasks ~= t;
            save();
        } else {
            printHelp("Task already running or no tasks found to continue");
        }
    } else if (command == "list") {
        load();
        foreach(i,t; tasks) {
            if (t.Type == TaskType.Start) {
                string name = t.Name;
                auto startTime = SysTime.fromISOExtString(t.StartTime);
                if (tasks.length >= i+2) {
                    auto endTime = SysTime.fromISOExtString(tasks[i+1].StartTime);
                    auto diff = endTime - startTime;
                    writeln(
                        startTime.day.to!string
                        ~ "."
                        ~ startTime.month.to!string
                        ~ "."
                        ~ startTime.year.to!string
                        ~ " "
                        ~ startTime.hour.to!string
                        ~ ":"
                        ~ startTime.minute.to!string
                        ~ ":"
                        ~ startTime.second.to!string
                        ~ rightJustify(
                            ":: "
                            ~ name,
                            2, ' '
                        )
                        ~ rightJustify(
                            "=> "
                            ~ durationToString(diff),
                            2, ' '
                        )
                    );
                } else {
                    writeln(name ~ ": currently running. since: " ~ startTime.toString());
                }
            }
        }
        writeln("Press enter to exit...");
        waitForInput();
    } else if (command == "path") {
        if (exists(fileName)) {
            writeln(fileName);
        } else {
            writeln("taskfile not initialized.");
            writeln("try running 'task init' first");
        }
        waitForInput();
    } else {
        printHelp("Unknown command");
    }
}
